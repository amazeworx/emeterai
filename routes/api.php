<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\FilepondController;
use App\Http\Controllers\API\DropzoneController;
use App\Http\Controllers\API\PeruriController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [UserController::class, 'login']);
Route::post('register', [UserController::class, 'register']);
Route::post('logout', [UserController::class, 'logout'])->middleware('auth:sanctum');

Route::post('upload', [FilepondController::class, 'store']);
Route::delete('upload', [FilepondController::class, 'delete']);

Route::post('dropzone', [DropzoneController::class, 'store']);

Route::post('get-balance', [PeruriController::class, 'getBalance']);

Route::get('estamp-jobs', [PeruriController::class, 'getEstampJobs']);
Route::get('estamp-records', [PeruriController::class, 'getEstampRecords']);

Route::post('estamp-queue', [PeruriController::class, 'addToQueue']);
Route::delete('estamp-queue/delete/{id}', [PeruriController::class, 'removeQueue']);
Route::delete('estamp-queue/delete-all', [PeruriController::class, 'clearQueue']);
Route::post('estamp-queue/download/{id}', [PeruriController::class, 'setDownloadLink']);
Route::get('estamp-queue/download/{id}', [PeruriController::class, 'getDownloadDoc']);
Route::get('estamp-queue/download-selected', [PeruriController::class, 'zipSelectedDoc']);

Route::post('peruri-login', [PeruriController::class, 'peruriLogin']);
Route::post('peruri-upload', [PeruriController::class, 'uploadDoc']);
Route::post('generate-sn', [PeruriController::class, 'generateSn']);
Route::post('stamping', [PeruriController::class, 'stamping']);
Route::post('peruri-download', [PeruriController::class, 'downloadDoc']);
Route::delete('delete-selected', [PeruriController::class, 'deleteSelectedDoc']);

Route::post('get-doc-id', [PeruriController::class, 'getDocId']);
Route::get('test-download', [PeruriController::class, 'testDownload']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
  return $request->user();
});
