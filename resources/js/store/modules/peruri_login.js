const state = () => ({
  login_token: ""
})

const getters = {
  login_token: (state) => state.login_token,
}

const actions = {}

const mutations = {
  SET_LOGIN_TOKEN(state, payload) {
    state.login_token = payload;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}