let actions = {
  resetFormState({ commit }) {
    commit('RESET_STATE')
  },
  addToQueue({ commit }, post) {
    //console.log(post);
    return new Promise((resolve, reject) => {
      axios.post('/api/estamp-queue', post).then(res => {
        resolve(res.data);
      }, error => {
        reject(err);
      })
    });
  },
  peruriLogin({ commit }, post) {
    //console.log(post);
    return new Promise((resolve, reject) => {
      axios.post('/api/peruri-login', post).then(res => {
        resolve(res.data);
      }, error => {
        reject(err);
      })
    });
  },
  peruriUploadDoc({ commit }, post) {
    //console.log(post);
    return new Promise((resolve, reject) => {
      axios.post('/api/peruri-upload', post).then(res => {
        resolve(res.data);
      }, error => {
        reject(err);
      })
    });
  },
  peruriGetDocId({ commit }, post) {
    //console.log(post);
    return new Promise((resolve, reject) => {
      axios.post('/api/get-doc-id', post).then(res => {
        resolve(res.data);
      }, error => {
        reject(err);
      })
    });
  },
  peruriGenerateSn({ commit }, post) {
    //console.log(post);
    return new Promise((resolve, reject) => {
      axios.post('/api/generate-sn', post).then(res => {
        resolve(res.data);
      }, error => {
        reject(err);
      })
    });
  },
  peruriStamping({ commit }, post) {
    //console.log(post);
    return new Promise((resolve, reject) => {
      axios.post('/api/stamping', post).then(res => {
        resolve(res.data);
      }, error => {
        reject(err);
      })
    });
  },
  peruriDownload({ commit }, post) {
    //console.log(post);
    return new Promise((resolve, reject) => {
      axios.post('/api/peruri-download', post).then(res => {
        resolve(res.data);
      }, error => {
        reject(err);
      })
    });
  },
  createPost({ commit }, post) {
    //console.log(post);
    axios.post('/api/vue-open-account', post)
      .then(res => {
        //console.log(res.data);
        //commit('CREATE_POST', res.data)
      }).catch(err => {
        console.log(err)
      })
  },
  requestOtp({ commit }, post) {
    //console.log(post);
    return new Promise((resolve, reject) => {
      axios.post('/api/otp/request', post).then(res => {
        resolve(res.data);
      }, error => {
        reject(err);
      })
    });
  },
  resendOtp({ commit }, post) {
    //console.log(post);
    return new Promise((resolve, reject) => {
      axios.post('/api/otp/resend', post).then(res => {
        resolve(res.data);
      }, error => {
        reject(err);
      })
    });
  },
  validateOtp({ commit }, post) {
    //console.log(post);
    return new Promise((resolve, reject) => {
      axios.post('/api/otp/validate', post).then(res => {
        resolve(res.data);
      }, error => {
        reject(err);
      })
    });
  },
  createLead({ commit }, post) {
    //console.log(post);
    return new Promise((resolve, reject) => {
      axios.post('/api/lead', post).then(res => {
        resolve(res.data);
      }, error => {
        reject(err);
      })
    });
  },
  createDraftAccount({ commit }, post) {
    //console.log(post);
    return new Promise((resolve, reject) => {
      axios.post('/api/vue-open-account/draft', post).then(res => {
        resolve(res.data);
      }, error => {
        reject(err);
      })
    });
  },
  updateDraftAccount({ commit }, post) {
    //console.log(post);
    return new Promise((resolve, reject) => {
      axios.post('/api/vue-open-account/update', post).then(res => {
        resolve(res.data);
      }, error => {
        reject(err);
      })
    });
  },
  fetchPosts({ commit }) {
    axios.get('/api/vue-open-account')
      .then(res => {
        //commit('FETCH_POSTS', res.data)
      }).catch(err => {
        console.log(err)
      })
  },
  deletePost({ commit }, post) {
    axios.delete(`/api/vue-open-account/${post.id}`)
      .then(res => {
        // if (res.data === 'ok') commit('DELETE_POST', post)
      }).catch(err => {
        console.log(err)
      })
  }
}

export default actions