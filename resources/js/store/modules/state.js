export const getDefaultState = () => {
  return {
    fields: {
    },
    upload_docs: [],
    login_token: "",
  }
}

const state = getDefaultState();

export default state