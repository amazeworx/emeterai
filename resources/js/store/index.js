import { createStore, createLogger } from 'vuex'
import state from "./modules/state";
import actions from './modules/actions'
import mutations from './modules/mutations'
import getters from './modules/getters'
import createPersistedState from "vuex-persistedstate";
//import peruriLogin from './modules/peruri_login'

const debug = process.env.NODE_ENV !== 'production'

export default createStore({
  state,
  actions,
  mutations,
  getters,
  modules: {
  },
  //strict: debug,
  //plugins: debug ? [createLogger()] : []
  plugins: [createPersistedState({ key: 'emeterai' })],
})