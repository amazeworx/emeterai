//https://shouts.dev/laravel-spa-with-vue3-auth-crud-example

import { createApp } from 'vue'
import VueLoading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';

// import DropZone from 'dropzone-vue';
// import 'dropzone-vue/dist/dropzone-vue.common.css';

require('./bootstrap')
import App from './App.vue'
import axios from 'axios'
import router from './router'
import store from './store';

//const VueWait = createVueWait()
const app = createApp(App)
app.config.globalProperties.$axios = axios;
app.use(router)
app.use(store)
app.use(VueLoading);
app.mount('#app')