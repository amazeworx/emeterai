import { createWebHistory, createRouter } from "vue-router";

//import Home from '../pages/Home';
//import About from '../pages/About';
import Register from '../pages/Register';
import Login from '../pages/Login';
//import Login from '../components/Login';
import Dashboard from '../pages/Dashboard';
import Records from '../pages/Records';
import Filepond from '../pages/Filepond';

export const routes = [
  {
    name: 'home',
    path: '/',
    component: Login
  },
  {
    name: 'register',
    path: '/register',
    component: Register
  },
  {
    name: 'login',
    path: '/login',
    component: Login
  },
  {
    name: 'dashboard',
    path: '/dashboard',
    component: Dashboard
  },
  {
    name: 'records',
    path: '/records',
    component: Records
  },
  {
    name: 'fileponds',
    path: '/fileponds',
    component: Filepond
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes: routes,
});

export default router;
