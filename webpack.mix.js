const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */



// mix.browserSync({
//   //proxy: process.env.APP_URL,
//   proxy: emeterai.local,
//   port: 8000,
//   notify: false
// });
mix.browserSync({
  //proxy: process.env.APP_URL,
  proxy: "localhost:8000",
  notify: false,
  files: ['public/js/**/*.js', 'public/css/**/*.css'],
});
mix.js('resources/js/app.js', 'public/js').vue();
mix.postCss('resources/css/app.css', 'public/css', [
  require('tailwindcss'),
]);
// mix.webpackConfig({
//   resolve: {
//     fallback: {
//       fs: false
//     }
//   }
// });