<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstampJobsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('estamp_jobs', function (Blueprint $table) {
      $table->id();
      $table->string('upload_name');
      $table->string('upload_id');
      $table->string('upload_err_code');
      $table->string('upload_message');
      $table->string('upload_link_url');
      $table->string('sn_result');
      $table->string('sn_filename_qr');
      $table->string('sn_err_code');
      $table->string('sn_message');
      $table->string('stamp_err_code');
      $table->string('stamp_message');
      $table->string('stamp_url_file');
      $table->string('estamp_job_status');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('estamp_jobs');
  }
}
