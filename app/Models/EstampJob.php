<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstampJob extends Model
{
  use HasFactory;
  protected $table = 'estamp_jobs';
  protected $guarded = [];
}
