<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\TemporaryFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FilepondController extends Controller
{
  public function store(Request $request)
  {
    if ($request->hasFile('upload_docs')) {
      $file = $request->file('upload_docs');
      $name = $file->getClientOriginalName();
      //$extension = $file->getClientOriginalExtension();
      $folder = uniqid() . '-' . now()->timestamp;
      $file->storePubliclyAs('uploads/tmp/' . $folder, $name, 'public');
      $storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
      $filepath = $storagePath . 'uploads/tmp/' . $folder . '/' . $name;

      TemporaryFile::create([
        'folder' => $folder,
        'filename' => $name,
        'filepath' => $filepath
      ]);

      return $folder;
    }
    return '';
  }

  public function delete(Request $request)
  {
    $temp_folder = $request->getContent();
    Storage::deleteDirectory('/public/uploads/tmp/' . $temp_folder);
    return $temp_folder;
  }
}
