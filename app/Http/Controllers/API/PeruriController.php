<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\TemporaryFile;
use App\Models\EstampJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use File;
use ZipArchive;

class PeruriController extends Controller
{
  public function getBalance(Request $request)
  {
    $token = $request->token;
    $csrf_token = $request->csrf_token;

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://backendservicedev.scm.perurica.co.id/function/saldopos',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_HTTPHEADER => array(
        'csrf-token: ',
        'Authorization: Bearer ' . $token
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
    //return response()->json($response);
  }

  public function getEstampRecords()
  {
    //$data = EstampJob::all();
    $data = EstampJob::where('estamp_job_status', 'completed')->get();
    return response()->json($data);
  }
  public function getEstampJobs()
  {
    $data = EstampJob::where('estamp_job_status', 'waiting')->get();
    //$data = EstampJob::all();
    return response()->json($data);
  }

  public function addToQueue(Request $request)
  {
    $folder = $request->folder;
    $data = TemporaryFile::where('folder', $folder)->first();

    EstampJob::create([
      'folder' => $folder,
      'upload_name' => $data->filename,
      'estamp_job_status' => 'waiting',
    ]);

    TemporaryFile::where('folder', $folder)->delete();

    // $tmp_path = public_path() . '/storage/uploads/tmp/' . $folder;
    // $queue_path = public_path() . '/storage/uploads/queue/' . $folder;
    $tmp_path = storage_path() . '/app/public/uploads/tmp/' . $folder;
    $queue_path = storage_path() . '/app/public/uploads/queue/' . $folder;

    File::moveDirectory($tmp_path, $queue_path);

    $response = [
      'success' => '200',
      'message' => 'File added to queue',
    ];

    return response()->json($response);
  }

  public function removeQueue($id)
  {
    $folder = EstampJob::where('id', $id)->value('folder');
    //$queue_path = public_path() . '/storage/uploads/queue/' . $folder;
    $queue_path = storage_path() . '/app/public/uploads/queue/' . $folder;
    File::deleteDirectory($queue_path);

    EstampJob::destroy($id);
    //return response()->noContent();
    return response()->json($folder);
  }

  public function clearQueue()
  {
    $data = EstampJob::where('estamp_job_status', 'waiting')->get();
    foreach ($data as $key) {
      $folder = EstampJob::where('id', $key->id)->value('folder');
      $queue_path = storage_path() . '/app/public/uploads/queue/' . $folder;
      File::deleteDirectory($queue_path);
      EstampJob::destroy($key->id);
    }
    //return 'Ok';
    return response()->noContent();
  }

  public function uploadDoc(Request $request)
  {
    $id = $request->id;
    $folder = $request->folder;
    $filename = $request->filename;
    $token = $request->token;

    $queue_path = storage_path() . '/app/public/uploads/queue/' . $folder . '/' . $filename;

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://fileuploaddev.scm.perurica.co.id/uploaddoc2',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => array('file' => new \CURLFILE($queue_path), 'token' => $token),
      CURLOPT_HTTPHEADER => array(
        'Authorization: Bearer ' . $token
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    $res = json_decode($response, true);

    //$status = $res['status'];
    $status_code = $res['statusCode'];
    //$message = $res['message'];
    $save_as = $res['saveAs'];
    $upload_id = $res['id'];
    //$name = $res['name'];
    //$only_name = $res['onlyName'];
    $upload_link_url = $res['linkUrl'];

    if ($status_code == "00") {
      EstampJob::where('id', $id)->update([
        'upload_id' => $upload_id,
        'upload_link_url' => $upload_link_url,
      ]);
    }

    $data = EstampJob::where('id', $id)->first();
    $tgl_doc = date('Y-m-d', strtotime($data->created_at));

    $json = [
      'id' => $id,
      'statusCode' => $status_code,
      'uploadId' => $upload_id,
      'saveAs' => $save_as,
      'noDoc' => $data->id,
      'tglDoc' => $tgl_doc
    ];

    //$response = response($response)->header('Content-type', 'application/json');
    //return $response;
    return response()->json($json);
  }

  public function generateSn(Request $request)
  {
    $id = $request->id;
    $token = $request->token;
    $idfile = $request->idfile;
    $namafile = $request->namafile;
    $nodoc = $request->nodoc;
    $tgldoc = $request->tgldoc;

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://stampv2dev.scm.perurica.co.id/chanel/stampv2',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => '{
          "idfile": "' . $idfile . '",
          "isUpload": true,
          "namadoc": "Dokumen Transaksi",
          "namafile": "' . $namafile . '",
          "nilaidoc": "10000",
          "snOnly": false,
          "nodoc": "' . $nodoc . '",
          "tgldoc": "' . $tgldoc . '"
      }',
      CURLOPT_HTTPHEADER => array(
        'Authorization: Bearer ' . $token,
        'Content-Type: application/json'
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    $res = json_decode($response, true);

    if ($res['statusCode'] == "00") {
      EstampJob::where('id', $id)->update([
        'sn_result' => $res['result']['sn'],
        'sn_filename_qr' => $res['result']['filenameQR'],
      ]);
    }

    $json = [
      'id' => $id,
      'statusCode' => $res['statusCode'],
      'serialNumber' => $res['result']['sn'],
      'fileQr' => $res['result']['filenameQR']
    ];

    //return $response;
    return response()->json($json);
  }

  public function stamping(Request $request)
  {
    $id = $request->id;
    $token = $request->token;
    $docId = $request->docId;
    $dest = $request->dest;
    $refToken = $request->refToken;
    $spesimenPath = $request->spesimenPath;
    $src = $request->src;

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://stampservicedev.scm.perurica.co.id/keystamp/adapter/docSigningZ',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => '{
          "onPrem": false,
          "docId": "' . $docId . '",
          "certificatelevel": "NOT_CERTIFIED",
          "dest": "' . $dest . '",
          "docpass": "",
          "jwToken": "' . $token . '",
          "location": "JAKARTA",
          "profileName": "emeteraicertificateSigner",
          "reason": "Dokumen Transaksi",
          "refToken": "' . $refToken . '",
          "spesimenPath": "' . $spesimenPath . '",
          "src": "' . $src . '",
          "visLLX": 693,
          "visLLY": 5,
          "visURX": 748,
          "visURY": 55,
          "visSignaturePage": 1
      }',
      CURLOPT_HTTPHEADER => array(
        'Authorization: Bearer ' . $token,
        'Content-Type: application/json'
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    $res = json_decode($response, true);

    if ($res['errorCode'] == "00") {
      EstampJob::where('id', $id)->update([
        'stamp_url_file' => $res['urlFile'],
        'estamp_job_status' => 'completed'
      ]);
    }

    $json = [
      'id' => $id,
      'statusCode' => $res['errorCode'],
      'urlFile' => $res['urlFile'],
    ];

    //return $response;
    return response()->json($json);
  }

  public function downloadDoc(Request $request)
  {
    $id = $request->id;
    $token = $request->token;
    $srcfileStamp = $request->srcfileStamp;

    $fileName = 'final_' . EstampJob::where('id', $id)->value('upload_name');

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $srcfileStamp,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/pdf',
        'Authorization: Bearer ' . $token
      ),
    ));

    $response = curl_exec($curl);

    $final_response = response($response)->header('Content-type', 'application/pdf')->header('Content-Disposition', 'attachment; filename="' . $fileName . '"');

    $jobData = EstampJob::where('id', $id)->first();
    $folder = $jobData->folder;
    $year = date('Y', strtotime($jobData->created_at));
    $month = date('m', strtotime($jobData->created_at));
    $day = date('d', strtotime($jobData->created_at));
    // $filepath = public_path() . '/storage/downloads/' . $year . '/' . $month . '/' . $day . '/';
    $filepath = storage_path() . '/app/public/downloads/' . $year . '/' . $month . '/' . $day . '/';
    File::ensureDirectoryExists('storage/downloads/' . $year . '/' . $month . '/' . $day);
    $fileDest = $filepath . $fileName;
    file_put_contents($fileDest, $final_response);
    // $queue_path = public_path() . '/storage/uploads/queue/' . $folder;
    $queue_path = storage_path() . '/app/public/uploads/queue/' . $folder;
    File::deleteDirectory($queue_path);

    $downloadUrl = '/storage/downloads/' . $year . '/' . $month . '/' . $day . '/' . $fileName;
    EstampJob::where('id', $id)->update([
      'download_url' => $downloadUrl,
    ]);

    curl_close($curl);

    //return $final_response;
    return 'Success';
  }

  public function getDownloadDoc($id)
  {
    $jobData = EstampJob::where('id', $id)->first();
    $fileName = 'final_' . $jobData->upload_name;
    $year = date('Y', strtotime($jobData->created_at));
    $month = date('m', strtotime($jobData->created_at));
    $day = date('d', strtotime($jobData->created_at));
    $downloadUrl = '/storage/downloads/' . $year . '/' . $month . '/' . $day . '/' . $fileName;
    //return response()->json($download_path);
    $json = [
      'fileName' => $fileName,
      'downloadUrl' => $downloadUrl
    ];
    //return $downloadUrl;
    return response()->json($json);
  }

  public function zipSelectedDoc(Request $request)
  {
    //$files = $request->files;

    $time = date('Ymd-His');

    $zip = new ZipArchive;
    $zip_file = 'TCN-' . $time . '.zip';

    $zip->open(storage_path('/app/public/zip/' . $zip_file), ZipArchive::CREATE | ZipArchive::OVERWRITE);

    //$invoice_file = public_path() . '/storage/downloads/2022/02/25/final_TCN-6KA030-13Jan2022.pdf';

    //$zip->addFile($invoice_file, 'final_TCN-6KA030-13Jan2022.pdf');

    $files = $request->input('files');

    foreach ($files as $key => $value) {
      $download_url = EstampJob::where('id', $value)->value('download_url');
      //$download_file = public_path() . EstampJob::where('id', $value)->value('download_url');
      $download_file = storage_path('/app/public' . str_replace("/storage/", "/", $download_url));
      $file_name = 'final_' . EstampJob::where('id', $value)->value('upload_name');
      //$download_urls[] = public_path($download_url);
      //$zip->addFile($download_url);
      $zip->addFile($download_file, $file_name);
    }

    $zip->close();

    $downloadUrl = '/storage/zip/' . $zip_file;

    $json = [
      'fileName' => $zip_file,
      'downloadUrl' => $downloadUrl
    ];

    return response()->json($json);
  }

  public function deleteSelectedDoc(Request $request)
  {

    $files = $request->input('files');

    foreach ($files as $key => $id) {
      $download_url = EstampJob::where('id', $id)->value('download_url');
      $download_file = storage_path('/app/public' . str_replace("/storage/", "/", $download_url));
      File::delete($download_file);
      EstampJob::destroy($id);
    }

    return 'Ok';
  }

  public function getDocId(Request $request)
  {
    // UNUSED
    $data = TemporaryFile::where('folder', $request->folder)->first();
    $tgldoc = date('Y-m-d', strtotime($data->created_at));
    $json = [
      'nodoc' => $data->id,
      'tgldoc' => $tgldoc
    ];
    return response()->json($json);
  }

  public function testDownload()
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_SSL_VERIFYPEER => false,
      CURLOPT_URL => 'https://fileuploaddev.scm.perurica.co.id/getfile3/final_3af995e7-33e0-43ad-8379-a805d5deb58f.pdf',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/pdf',
        'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6ImFjY2VzcyJ9.eyJlbWFpbCI6InNlbWVzdGFfaW5kb3Zlc3RAeW9wbWFpbC5jb20iLCJpYXQiOjE2NDQxNzIzMDMsImV4cCI6MTY0NDI1ODcwMywiYXVkIjoiaHR0cHM6Ly95b3VyZG9tYWluLmNvbSIsImlzcyI6ImZlYXRoZXJzIiwic3ViIjoiNjE3NjVlYWZmYzIyYjMwMDI4ZWVhNTU5IiwianRpIjoiMDhkNmQ0MmMtYWMwMC00MjFlLWJmOTgtZmJkMWEwOWFlNjU5In0.P29LWgKy40Rm3Ig5DiphJ3FGUxvisYT6hQyomi2X3Aw'
      ),
    ));


    $response = curl_exec($curl);
    $final_response = response($response)->header('Content-type', 'application/pdf')->header('Content-Disposition', 'attachment; filename ="test.pdf"');

    $storagePath  = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
    $filepath = $storagePath . 'downloads/';
    $filename = $filepath . 'test.pdf';
    file_put_contents($filename, $final_response);
    //header('Content-type: application/pdf');
    //header('Content-Disposition: attachment; filename ="test.pdf"');

    curl_close($curl);
    //return $response;

    return $final_response;
    //return 'Hello';
  }
}
