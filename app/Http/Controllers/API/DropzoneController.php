<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\TemporaryFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\EstampJob;

class DropzoneController extends Controller
{
  public function store(Request $request)
  {
    // if ($request->hasFile('upload_docs')) {
    //   $file = $request->file('upload_docs');
    //   $name = $file->getClientOriginalName();
    //   //$extension = $file->getClientOriginalExtension();
    //   $folder = uniqid() . '-' . now()->timestamp;
    //   $file->storePubliclyAs('uploads/tmp/' . $folder, $name, 'public');
    //   $storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
    //   $filepath = $storagePath . 'uploads/tmp/' . $folder . '/' . $name;

    //   TemporaryFile::create([
    //     'folder' => $folder,
    //     'filename' => $name,
    //     'filepath' => $filepath
    //   ]);

    //   return $folder;
    // }
    //return '';

    // if ($request->hasFile('upload_docs')) {
    //   $file = $request->file('upload_docs');
    //   $name = $file->getClientOriginalExtension();
    //   $folder = uniqid() . '-' . now()->timestamp;
    //   $file->storePubliclyAs('uploads/queue/' . $folder, $name, 'public');

    //   //return response()->json(['success' => 'You have successfully upload file.']);

    //   return $folder;
    // }
    // return '';

    //$req = $request;
    $file = $request->file;
    //$name = $file->getClientOriginalExtension();
    $name = $file->getClientOriginalName();
    $folder = uniqid() . '-' . now()->timestamp;
    $file->storePubliclyAs('uploads/queue/' . $folder, $name, 'public');

    EstampJob::create([
      'folder' => $folder,
      'upload_name' => $name,
      'estamp_job_status' => 'waiting',
    ]);

    //return response()->json(['success' => 'You have successfully upload file.']);

    //return $file;
    return response()->json([
      'name' => $name,
      'folder' => $folder,
    ]);
  }
}
